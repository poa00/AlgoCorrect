﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance, Force

VERSION := "v0.0.1"

;#Include src\AlgoCorrect.ahk

;inputTracker := new AlgoCorrect.Controller.InputTracker()
ts := ComObjCreate("Python.BasicServer")

^+x::msgbox % ts.ping()

;analyzeInputFn := ObjBindMethod(inputTracker, "analyzeInput")
;Hotkey, ^+x, % analyzeInputFn

return
